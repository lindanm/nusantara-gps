<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Arvo:wght@700&family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" integrity="sha512-f8gN/IhfI+0E9Fc/LKtjVq4ywfhYAVeMGKsECzDUHcFJ5teVwvKTqizm+5a84FINhfrgdvjX8hEJbem2io1iTA==" crossorigin="anonymous" />
    <link href="{{ asset('landing/owl_carousel/dist/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('landing/owl_carousel/dist/assets/owl.theme.default.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link href="{{ asset('landing/font/arvo-bold.ttf') }}" type="text/truetype">
    <link rel="stylesheet" href="{{ asset('landing/css/style.css') }}">

    <title>@yield('title')</title>
  </head>

  <body data-spy="scroll" data-target=".navbar-light">
    <section id="navbar">
      <nav class="navbar navbar-expand-lg navbar-light fixed-top">
        <div class="container">
          <div class="logo">
            <div class="row">
              <a class="navbar-brand mr-auto" href="#"><img src="{{ asset('/landing/img/nusantaraGPS.png') }}"></a>
            </div>
            <div class="row hidden-sm">
              <p><a href="mailto:lintasjejak@gmail.com">lintasjejak@gmail.com</a> | <a href="https://wa.me/6281334170440?text=Kontak%20dari%20website%20NusantaraGPS">0813-3417-0440</a></p>
            </div>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="#home">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#profile">Profile</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#product">Produk</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#contact">Kontak</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Login</a>
                </li>
              </ul>
            </div>
        </div>
      </nav>
    </section>

    @yield('container')
    
    <section id="footer">
      <div class="container">
        <div class="footer-bg">
          <svg width="1440" height="215" viewBox="0 0 1440 215" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M891.152 92.8789C1239.79 0.422241 1402.32 20.933 1440 42.7455V214.5L0 214.5V6.10352e-05C24.8276 14.2485 182.971 69.1632 305.326 92.8789C529.925 136.412 799.765 119.617 891.152 92.8789Z" fill="#75E195"/>
          </svg>
          <!-- <svg viewBox="0 0 1440 191" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M891.152 71.7738C1239.79 0.326508 1402.32 16.1766 1440 33.0325V191L0 191V0.000305176C24.8276 11.011 182.998 53.4463 305.326 71.7738C529.959 105.429 799.765 92.4359 891.152 71.7738Z" fill="url(#paint0_linear)"/>
            <defs>
              <linearGradient id="paint0_linear" x1="720" y1="191" x2="720" y2="95" gradientUnits="userSpaceOnUse">
                <stop offset="0.353901" stop-color="#55C595"/>
                <stop offset="1" stop-color="#7CE495"/>
              </linearGradient>
            </defs>
          </svg>        -->
        </div>
        <div class="footer-wrapper">
          <div class="row">
            <div class="col-md-7">
              <div class="footer-left mr-auto">
                <h3>NUSANTARA GPS</h3>
                <p>Jl. Akordion, Tunggulwulung, Kec. Lowokwaru, Kota Malang, Jawa Timur, 65143</p>
                <p><a href="https://wa.me/6281334170440?text=Kontak%20dari%20website%20NusantaraGPS">0813-3417-0440</a> | <a href="mailto:lintasjejak@gmail.com">lintasjejak@gmail.com</a> </p>
              </div>
            </div>
            <div class="col-md-5">
              <div class="footer-right text-md-right">
                <ul  class="hidden-sm">
                  <li>
                    <a href="#">Home</a>
                  </li>
                  <li>
                    <a href="#">Profil</a>
                  </li>
                  <li>
                    <a href="#">Produk</a>
                  </li>
                  <li>
                    <a href="#">Kontak</a>
                  </li>
                  <li>
                    <a href="#">Login</a>
                  </li>
                </ul>
                <p>2020 - PT. Lintas Jejak Nusaraya. All right reserved</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="https://code.iconify.design/1/1.0.6/iconify.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js" type="text/javascript" ></script>
    <!-- <script src="{{ asset('landing/owl_carousel/docs/assets/vendors/jquery.min.js') }}" type="text/javascript"></script> -->
    <script src="{{ asset('landing/owl_carousel/dist/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('discount/js/Winwheel.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('landing/js/mainjs.js') }}" type="text/javascript"></script>
    <script src="{{ asset('landing/js/show-on-scroll.js') }}" type="text/javascript"></script>

    @yield('script')

  </body>
</html>