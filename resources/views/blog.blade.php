@extends('landing/templates/header')

@section('title', 'Blog | Nusantara GPS')

@section('container')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="article-body pr-5">
                    <h2 class="text-left">Manfaatkan GPS, Polisi tangkap perampas mobil di Serang</h2>
                    <p class="my-3">Selasa, 12 Agustus 2020 07:14</p>
                    <img src="{{ asset('blog/img/cover/polisi.png') }}" class="img-fluid text-center my-4">
                    <p>Kepolisian Polda Metro Jaya bekerja sama dengan jajaran Mapolres Serang, Banten, menangkap pelaku pencurian mobil dengan memanfaatkan teknologi GPS (global positioning system).</p>
                    <p>Akun Facebook TMC Polda Metro Jaya melaporkan pemilik mobil, Anggi mengalami tindak kejahatan perampasan pada Jumat (26/8/2016) lalu ketika tengah berhenti di kawasan Kemang Pratama V, Bekasi. Saat itu, Anggi bersama saudaranya sedang bepergian ke daerah tersebut.</p>
                    <p>"Ibu Anggi melaporkan kepada AKBP Timin (Kasat Lantas Jakarta Timur). Timin segera menghubungi TMC Polda Metro Jaya untuk melakukan pelacakan. Jajaran anggota juga melakukan pengejaran," kata Kepala Sub Bagian Tekinfo Polda Metro Jaya, Kompol Purwono.</p>
                    <p>Mobil Anggi menurut keterangan, telah dipasangi GPS yang langsung bisa dilacak. Dari hasil pantauan GPS, Petugas TMC Polda Metro Jaya menemukan titik koordinat mobil yang dirampas pelaku, yakni sedang berada di Jalan Ahmad Yani, Serang, Banten pada Sabtu (27/8/2016).</p>
                    <p>Petugas TMC Polda Metro Jaya meneruskan informasi tersebut ke Polres Serang dan diterima oleh Bripka Wahyu. Oleh Bripka Wahyu langsung dilakukan pengejaran terhadap pelaku.</p>
                    <p>"Dengan dipandu oleh Petugas TMC Polda Metro Jaya, Bripka Wahyu kemudian menyergap pelaku dan mengamankan barang bukti ke Polres Serang," jelas Purwono.</p>
                    <p>Peristiwa ini menunjukkan efektifnya teknologi GPS yang dipasang di mobil. Jika terjadi tindak pencurian atau perampasan, polisi bisa dengan mudah mencari lokasi keberadaan mobil yang dicuri.</p>
                </div>                
            </div>
            <div class="col-md-4">
                <div class="article-list">
                    <h3>BERITA LAINNYA</h3>
                    <div class="card">
                        <div class="card-title">
                            <p>Selasa, 12 Agustus 2020  07:14</p>
                        </div>
                        <div class="card-body">
                            <h6>Susul As dan Rusia, China luncurkan satelit pendukung....</h6>
                            <a href="">Selengkapnya....</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-title">
                            <p>Selasa, 12 Agustus 2020  07:14</p>
                        </div>
                        <div class="card-body">
                            <h6>Susul As dan Rusia, China luncurkan satelit pendukung....</h6>
                            <a href="">Selengkapnya....</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-title">
                            <p>Selasa, 12 Agustus 2020  07:14</p>
                        </div>
                        <div class="card-body">
                            <h6>Susul As dan Rusia, China luncurkan satelit pendukung....</h6>
                            <a href="">Selengkapnya....</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-title">
                            <p>Selasa, 12 Agustus 2020  07:14</p>
                        </div>
                        <div class="card-body">
                            <h6>Susul As dan Rusia, China luncurkan satelit pendukung....</h6>
                            <a href="">Selengkapnya....</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-title">
                            <p>Selasa, 12 Agustus 2020  07:14</p>
                        </div>
                        <div class="card-body">
                            <h6>Susul As dan Rusia, China luncurkan satelit pendukung....</h6>
                            <a href="">Selengkapnya....</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection