@extends('landing/templates/header')

@section('title', 'Diskon | Nusantara GPS')

@section('container')
<section id="discount-hero">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset('discount/img/diskon-hero.png') }}" class="img-fluid my-auto">
            </div>
            <div class="col-md-6 hero-content">
                <h1>DISCOUNT</h1>
                <h1>UP TO <span>50%</span></h1>
                <p>Dapatkan diskon paket GPS dimulai dari 20% sampai 50%. Klik tombol dibawah ini untuk mendapatkan diskon produk Nusantara GPS</p>
                <a href="#roulette" class="btn btn-primary mt-4">PESAN SEKARANG</a>
            </div>
        </div>
    </div>
</section>

<section id="roulette">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <h2 class="show-on-scroll">ROULETTE</h2>
                <p>Masukkan nama dan no telepon anda. Putar roulette untuk mendapatkan diskon paket Nusantara GPS. Untuk konfirmasi, silahkan share diskon yang didapat melalui Whatsapp (0813-3417-0440)</p>
            </div>
            <div class="col-md-6 form-roulette my-4">
                <form action="#" method="POST" id="userform">
                    <div class="form-group">
                            <label for="input-nama">Nama</label>
                            <input type="text" class="form-control" id="input-nama" placeholder="Nama" required>
                        </div>
                    <div class="form-group">
                        <label for="input-telephone">Telephone</label>
                        <input type="number" class="form-control" id="input-telephone" placeholder="Telephone" required>
                    </div>
                    <div class="form-group">
                        <div class="button-msg-center text-center">
                            <button type="submit" class="btn btn-primary my-4">Kirim</button>
                        </div> 
                    </div>                                             
                </form>
            </div>
        </div>
        <div class="roulette text-center">
          <p>Tekan tombol putar untuk mendapatkan diskon hingga 50%.</p>
          <div class="row justify-content-center">
            <div class="canvasContainer">
                <div class="power_controls">
                    <img id="spin_button" src="{{ asset('discount/img/spin_on.png') }}" alt="Spin" onClick="startSpin();" class="clickable"/>                            
                </div>
                <canvas id="canvas" width="434" height="434">
                    <p>Sorry, your browser doesn't support canvas. Please try another.</p>
                </canvas>
                <div class="prize_pointer">
                    <img id="prizePointer" src="{{ asset('discount/img/arrow-left.png') }}" alt="V" width="80px"/>
                </div>                        
            </div>
          </div>
        </div>        
    </div>
</section>

<section id="price">
    <div class="container">
        <h2 class="show-on-scroll">HARGA PAKET GPS</h2>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-title text-center">
                        <h3>PAKET NORMAL</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 align-items-center">
                                <img src="{{ asset('discount/img/price.png') }}" class="img-fluid">
                            </div>
                            <div class="col-md-6">
                                <h5>Rp. 2.500.000,-</h5>
                                <p><span>Fitur yang didapatkan</span><br>1 Alat GPS<br>Gratis Biaya Server Selamanya<br>Gratis Kuota Telkomsel 1 Tahun<br>Garansi alat 1 Tahun</p>
                                <a href="#" class="btn btn-primary btn-order my-4">Pesan</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script>
            // Create new wheel object specifying the parameters at creation time.
            let theWheel = new Winwheel({
                'numSegments'  : 8,     // Specify number of segments.
                'outerRadius'  : 212,   // Set outer radius so wheel fits inside the background.
                'textFontSize' : 36,    // Set font size as desired.
                'textAlignment'  : 'outer',
                'textFontFamily'  : 'Montserrat',
                'textFillStyle' : '#FFFFFF',
                'strokeStyle' : 'transparent',
                'lineWidth' : '0px',
                'rotationAngle'   : -30,
                'pointerAngle' : 90,
                'responsive'   : true,
                'segments'     :        // Define segments including colour and text.
                [
                   {'fillStyle' : '#3B7164', 'text' : 'Zonk', },
                   {'fillStyle' : '#3C9C74', 'text' : '20%'},
                   {'fillStyle' : '#55C595', 'text' : '25%'},
                   {'fillStyle' : '#92E4BD', 'text' : '30%'},
                   {'fillStyle' : '#3B7164', 'text' : '35%'},
                   {'fillStyle' : '#3C9C74', 'text' : '40%'},
                   {'fillStyle' : '#55C595', 'text' : '45%'},
                   {'fillStyle' : '#92E4BD', 'text' : '50%'}
                ],
                'animation' :           // Specify the animation to use.
                {
                    'type'     : 'spinToStop',
                    'duration' : 8,     // Duration in seconds.
                    'spins'    : 8,     // Number of complete spins.
                    'callbackFinished' : alertPrize,stopSound,
                }
            });

            let wheelPower    = 0;
            let wheelSpinning = false;

            let audio = new Audio("{{ url('/discount/spinning.mp3') }}");  // Create audio object and load tick.mp3 file.

            function stopSound()
            {
                // Stop and rewind the sound if it already happens to be playing.
                audio.pause();
                audio.currentTime = 0;
            }
            

            // -------------------------------------------------------
            // Click handler for spin button.
            // -------------------------------------------------------
            function startSpin()
            {
                // Ensure that spinning can't be clicked again while already running.
                if (wheelSpinning == false) {
                    
                    // Disable the spin button so can't click again while wheel is spinning.
                    document.getElementById('spin_button').src       = "{{ asset('discount/img/spin_off.png') }}";
                    document.getElementById('spin_button').className = "clickable";

                    // Play the sound.
                    audio.play();

                    // Begin the spin animation by calling startAnimation on the wheel object.
                    theWheel.startAnimation();

                    // Set to true so that power can't be changed and spin button re-enabled during
                    // the current animation. The user will have to reset before spinning again.
                    wheelSpinning = true;
                }
            }

            // -------------------------------------------------------
            // Function for reset button.
            // -------------------------------------------------------
            function resetWheel()
            {
                theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
                theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
                theWheel.draw();                // Call draw to render changes to the wheel.

                wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
                document.getElementById('spin_button').src       = "{{ asset('discount/img/spin_on.png') }}";
                    document.getElementById('spin_button').className = "clickable";
            }

            function alertPrize(indicatedSegment)
            {
                // Do basic alert of the segment text. You would probably want to do something more interesting with this information.
                var indicatedSegment = indicatedSegment.text;

                if(indicatedSegment == 'Zonk') {
                    swal({
                        title: "Zonk!!!",
                        text: "Maaf, Anda Belum Beruntung",
                        icon: "error",
                        button: "Coba Lagi"
                    }).then(
                        function(isConfirm) {
                            resetWheel();
                        }
                    );
                } else {
                    var discount = parseInt(indicatedSegment.substr(0,2));
                    var finalPrice = (2500000-((discount/100)*2500000));
                    var reverse = finalPrice.toString().split('').reverse().join(''),
                        ribuan = reverse.match(/\d{1,3}/g);
                        ribuan = ribuan.join('.').split('').reverse().join('');
                    swal({
                        title: "Selamat!!!",
                        text: "Anda Mendapatkan Diskon Sebesar " + indicatedSegment + " dari harga total Rp 2.500.000. Anda hanya perlu membayar sejumlah Rp. " + ribuan +"\n\n Kirim screenshot hasil roulette yang anda peroleh untuk klaim diskon.",
                        icon: "success",
                        buttons: ["Coba Lagi", "Klaim Diskon"],
                    }).then(
                        function(isConfirm) {
                            if(isConfirm) {
                                window.location.href = "https://wa.me/6281334170440?text=Pemenang%20game%20diskon%20roulette%20dari%20website%20NusantaraGPS";
                            }else {
                                resetWheel();
                            }
                    });              
                }
            }
                
            
        </script>
@endsection
