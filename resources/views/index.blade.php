@extends('landing/templates/header')

@section('title', 'Nusantara GPS | Solusi Tracking Armada Anda')

@section('container')
<section id="home">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Nusantara GPS</h1>
                <h3>Solusi Tracking Armada Anda</h3>
                <p>Penyedia pelacak, aplikasi mobile sekaligus penyimpanan data berbasis teknologi GPS yang selalu menyesuaikan kebutuhan pelanggan akan pemantauan posisi aset/kendaraan/manusia.</p>
                <a href="#profile" class="btn btn-primary my-4">MULAI</a>
            </div>
            <div class="col-md-6 my-auto">
                <img class="img-fluid my-auto show-on-scroll" src="{{ asset('/landing/img/hero-landing.png') }}">
            </div>
        </div>
    </div>
</section>

<section id="profile">
    <div class="profile-bg">
        <svg viewBox="0 0 1440 215" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M891.152 92.8789C1239.79 0.422241 1402.32 20.933 1440 42.7455V214.5L0 214.5V6.10352e-05C24.8276 14.2485 182.971 69.1632 305.326 92.8789C529.925 136.412 799.765 119.617 891.152 92.8789Z" fill="#E2F3E3"/>
        </svg>
    </div>
    <div class="container">
        <div class="profile-top-wrapper">
            <div class="text-center">
                <p class="text-bold">Putar video dibawah ini untuk mengetahui lebih lanjut tentang Nusantara GPS.</p>
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="embed-responsive embed-responsive-16by9">
                            <video class="video-opening" controls>
                                <source src="{{ asset('landing/media/nusantaragps.mp4') }}">
                            </video>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-nav mx-auto">
                <a href="#" class="btn btn-primary btn-profile">
                    <span class="iconify" data-inline="false" data-icon="bx:bxs-discount"></span>
                    Diskon
                </a>
                <a href="#faq" class="btn btn-primary btn-profile">
                    <span class="iconify" data-inline="false" data-icon="wpf:faq"></span>
                    FAQ
                </a>
                <a href="#" class="btn btn-primary btn-profile">
                    <span class="iconify" data-inline="false" data-icon="dashicons:welcome-write-blog"></span>
                    Blog
                </a>
            </div>
        </div>
    </div>
</section>

<section id="visi-misi">
    <div class="container">
        <h2 class="show-on-scroll">VISI & MISI</h2>
        <p class="text-center col-md-10 mx-auto mb-5">Lintas jejak menjadi perusahaan penyedia solusi teknologi C4ISR (command, control, communication, computer, intelligence, surveillance, reconnaissance) nomor satu di Indonesia</p>
        <div class="row">
            <div class="col-md-4">
                <div class="misi">
                    <div class="row">
                        <div class="col-md-2">
                            <img src="{{ asset('landing/img/numbering_misi1.png') }}">
                        </div>
                        <div class="col-md-10">
                            <h5>Solusi teknologi terjangkau.</h5>
                            <p>Menciptakan solusi teknologi yang terjangkau untuk perusahaan yang tumbuh, berkembang serta memiliki visi yang maju</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="misi">
                    <div class="row">
                        <div class="col-md-2">
                            <img src="{{ asset('landing/img/numbering_misi2.png') }}">
                        </div>
                        <div class="col-md-10">
                            <h5>Solusi teknologi yang sesuai.</h5>
                            <p>Selalu melahirkan solusi teknologi yang sesuai dengan dinamika perkembangan industri global</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="misi">
                    <div class="row">
                        <div class="col-md-2">
                            <img src="{{ asset('landing/img/numbering_misi3.png') }}">
                        </div>
                        <div class="col-md-10">
                            <h5>Memahami kebutuhan mitra.</h5>
                            <p>Selalu menyerap dan memahami kebutuhan solusi mitra</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="service">
    <div class="container">
        <h2 class="show-on-scroll">KEUNGGULAN <br> NUSANTARA GPS</h2>
        <div class="carousel text-center">
            <div class="service-wrapper">
                <div class="card card-service">
                    <div class="card-body">
                        <img src="{{ asset('landing/img/service/service1.png') }}" class="img-fluid">
                        <h4>KREDIBEL</h4>
                        <p>Kami melayani segala segmen pelanggan dengan penuh komitmen dan mengutamakan kualitas</p>
                    </div>
                </div>
            </div>
            <div class="service-wrapper">
                <div class="card card-service">
                    <div class="card-body">
                        <img src="{{ asset('landing/img/service/service2.png') }}" class="img-fluid">
                        <h4>AKSES MUDAH</h4>
                        <p>Armada dapat dipantau melalui smartphone atau komputer</p>
                    </div>
                </div>
            </div>
            <div class="service-wrapper">
                <div class="card card-service">
                    <div class="card-body">
                        <img src="{{ asset('landing/img/service/service3.png') }}" class="img-fluid">
                        <h4>DAPAT DIANDALKAN</h4>
                        <p>Proses pantau yang dapat dilakukan kapan saja yang didukung bandwidth dan storage tak terbatas</p>
                    </div>
                </div>
            </div>
            <div class="service-wrapper">
                <div class="card card-service">
                    <div class="card-body">
                        <img src="{{ asset('landing/img/service/service4.png') }}" class="img-fluid">
                        <h4>VERSI ANDA</h4>
                        <p>Fitur dan tampilan yang dapat disesuaikan dengan kebutuhan dan selera</p>
                    </div>
                </div>
            </div>
            <div class="service-wrapper">
                <div class="card card-service">
                    <div class="card-body">
                        <img src="{{ asset('landing/img/service/service5.png') }}" class="img-fluid">
                        <h4>SINYAL KUAT</h4>
                        <p>Bekerja sama dengan provider GSM terkemuka di Indonesia untuk memastikan sistem berjalan sempurna</p>
                    </div>
                </div>
            </div>
            <div class="service-wrapper">
                <div class="card card-service">
                    <div class="card-body">
                        <img src="{{ asset('landing/img/service/service6.png') }}" class="img-fluid">
                        <h4>LAYANAN PELANGGAN</h4>
                        <p>Solusi dan penanganan masalah dengan cepat dan tepat melalui dukungan CS dan teknisi handal</p>
                    </div>
                </div>
            </div>
            <div class="service-wrapper">
                <div class="card card-service">
                    <div class="card-body">
                        <img src="{{ asset('landing/img/service/service7.png') }}" class="img-fluid">
                        <h4>TERDAFTAR HAKI</h4>
                        <p>Produk kami telah memiliki hak paten yang terdaftar pada HAKI (Hak Kekayaan Intelektual)</p>
                    </div>
                </div>
            </div>
            <div class="service-wrapper">
                <div class="card card-service">
                    <div class="card-body">
                        <img src="{{ asset('landing/img/service/service8.png') }}" class="img-fluid">
                        <h4>TERDAFTAR PKP</h4>
                        <p>Kami merupakan PKP (Pengusaha Kena Pajak) yang membantu ekonomi negara melalui pajak yang dibayarkan</p>
                    </div>
                </div>
            </div>
            <div class="service-wrapper">
                <div class="card card-service">
                    <div class="card-body">
                        <img src="{{ asset('landing/img/service/service9.png') }}" class="img-fluid">
                        <h4>BERGARANSI</h4>
                        <p>Kami menawarkan produk dengan garansi 1 tahun, anda tak perlu khawatir dengan keselamatan produk yang telah dibeli</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="features">
    <div class="container">
        <h2 class="show-on-scroll">LAYANAN <br> NUSANTARA GPS</h2>
        <div class="row">
            <div class="col-md-4">
                <img src="{{ asset('/landing/img/features/real_time_monitoring.png')}}" class="img-fluid show-on-scroll">
                <h4>REAL TIME MONITORING</h4>
                <p>Pantau informasi lengkap dan akurat posisi armada atau kendaraan Anda secara langsung.</p>
            </div>
            <div class="col-md-4">
                <img src="{{ asset('/landing/img/features/komprehensif.png')}}" class="img-fluid show-on-scroll">
                <h4>KOMPREHENSIF</h4>
                <p>Pantau lokasi, rute, kecepatan dan status kendaraan dengan mudah sesuai koordinat peta yang akurat.</p>
            </div>
            <div class="col-md-4">
                <img src="{{ asset('/landing/img/features/pagar_virtual.png')}}" class="img-fluid show-on-scroll">
                <h4>PAGAR VIRTUAL</h4>
                <p>Anda dapat menentukan batas pergerakan kendaraan ada dan dapatkan pemberitahuan dari sistem apabila kendaraan melewati batas tersebut.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 ml-auto">
                <img src="{{ asset('/landing/img/features/multi_track_system.png')}}" class="img-fluid show-on-scroll">
                <h4>MULTI TRACK SYSTEM</h4>
                <p>Cukup satu akun untuk memonitor banyak pelacak dalam satu waktu yang sama.</p>
            </div>
            <div class="col-md-4 mr-auto">
                <img src="{{ asset('/landing/img/features/arsip_lengkap.png')}}" class="img-fluid show-on-scroll">
                <h4>ARSIP LENGKAP</h4>
                <p>Seluruh rekaman data perjalanan, aktivitas, dan notifikasi yang berhubungan dengan pemantauan kendaraan akan tersimpan rapi dan sistematik.</p>
            </div>
        </div>
    </div>
</section>

<section id="product">
    <div class="container">
        <h2 class="show-on-scroll">PRODUK</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="card text-center">
                    <img src="{{ asset('/landing/img/product/gps_lj04.png') }}" class="img-fluid">
                    <div class="card-body">
                        <h5>GPS LJ04</h5>
                        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-product">Lihat Detail</a>
                        <a href="https://wa.me/6281334170440?text=Pesan%20produk%20GPS%20LJ04%20dari%20website%20NusantaraGPS" class="btn btn-primary">Pesan</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center">
                    <img src="{{ asset('/landing/img/product/gps_lj05.png') }}" class="img-fluid">
                    <div class="card-body">
                        <h5>GPS LJ05</h5>
                        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-product">Lihat Detail</a>
                        <a href="https://wa.me/6281334170440?text=Pesan%20produk%20GPS%20LJ05%20dari%20website%20NusantaraGPS" class="btn btn-primary">Pesan</a>
                    </div>
                </div>            
            </div>
            <div class="col-md-4">
                <div class="card text-center">
                    <img src="{{ asset('/landing/img/product/gps_lj06.png') }}" class="img-fluid">
                    <div class="card-body">
                        <h5>GPS LJ06</h5>
                        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-product">Lihat Detail</a>
                        <a href="https://wa.me/6281334170440?text=Pesan%20produk%20GPS%20LJ06%20dari%20website%20NusantaraGPS" class="btn btn-primary">Pesan</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 ml-auto">
                <div class="card text-center">
                    <img src="{{ asset('/landing/img/product/gps_ljmac.png') }}" class="img-fluid">
                    <div class="card-body">
                        <h5>GPS LJMAC</h5>
                        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-product">Lihat Detail</a>
                        <a href="https://wa.me/6281334170440?text=Pesan%20produk%20GPS%20LJMAC%20dari%20website%20NusantaraGPS" class="btn btn-primary">Pesan</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mr-auto">
                <div class="card text-center">
                    <img src="{{ asset('/landing/img/product/api.png') }}" class="img-fluid">
                    <div class="card-body">
                        <h5>API GPS</h5>
                        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-product">Lihat Detail</a>
                        <a href="https://wa.me/6281334170440?text=Pesan%20produk%20API%20GPS%20dari%20website%20NusantaraGPS" class="btn btn-primary">Pesan</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-product" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{ asset('/landing/img/product/gps_lj04.png') }}" class="img-fluid">
                            </div>
                            <div class="col-md-6">
                                <h5>GPS LJ04</h5>
                                <a href="https://wa.me/6281334170440?text=Pesan%20produk%20API%20GPS%20dari%20website%20NusantaraGPS" class="btn btn-primary">Pesan</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h6>Deskripsi</h6>
                                <p>Alat ini tahan banting, presisi, efisien dipenggunaan data GPRS. Biasanya digunakan untuk mobil dan truk.</p>
                            </div>
                            <div class="col-md-6">
                                <h6>Fitur</h6>
                                <p>Real-time GPS with GPRS/SMS<br>Acceleration sensor & vibration alarm<br>Ignition (ACC) detection<br>Built-in 450mAH battery<br>Tele cut-off engine function via SMS/WEB<br>SOS button<br>Voice monitor<br>Certification: EMARK,PC<br>Accuracy: 5-10 meter</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="faq">
    <div class="container">
        <h2 class="show-on-scroll">FAQ</h2>
        <div class="row">
            <div class="col-md-7">
            <div class="accordion md-accordion show-on-scroll" id="accordionFAQ">
                <div class="card">
                    <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Bagaimana cara kerja GPS? <i class="fas fa-angle-down rotate-icon"></i>
                        </button>
                    </h5>
                    </div>
            
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionFAQ">
                    <div class="card-body">
                        Sistem ini menggunakan sejumlah satelit yang berada di orbit bumi, yang memancarkan sinyalnya ke bumi dan ditangkap oleh sebuah alat penerima. Ada tiga bagian penting dari sistem ini, yaitu bagian kontrol, bagian angkasa, dan bagian pengguna.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Bagaimana akurasi alat navigasi GPS? <i class="fas fa-angle-down rotate-icon"></i>
                        </button>
                    </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionFAQ">
                    <div class="card-body">
                        Akurasi atau ketepatan perlu mendapat perhatian bagi penentuan koordinat sebuah titik/lokasi. Koordinat posisi ini akan selalu mempunyai 'faktor kesalahan', yang lebih dikenal dengan 'tingkat akurasi'. Misalnya, alat tersebut menunjukkan sebuah titik koordinat dengan akurasi 3 meter, artinya posisi sebenarnya bisa berada dimana saja dalam radius 3 meter dari titik koordinat (lokasi) tersebut. Makin kecil angka akurasi (artinya akurasi makin tinggi), maka posisi alat akan menjadi semakin tepat. Harga alat juga akan meningkat seiring dengan kenaikan tingkat akurasi yang bisa dicapainya.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Bagaimana cara pemesanan alat dari Nusantara GPS? <i class="fas fa-angle-down rotate-icon"></i>
                        </button>
                    </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionFAQ">
                    <div class="card-body">
                        Customer dapat memilih produk yang akan dibeli, kemudian pemesanan alat dari Nusantara GPS dapat dilakukan secara langsung dengan menghubungi nomor (0813-3417-0440) melalui Whatsapp atau dapat dilakukan dengan mengklik tombol pesan yang tertera pada halaman product website ini.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour">
                    <h5 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        Apa keunggulan fitur Nusantara GPS dibanding yang lainnya? <i class="fas fa-angle-down rotate-icon"></i>
                        </button>
                    </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionFAQ">
                    <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive">
                    <h5 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        Mengapa perlu menggunakan GPS untuk armada kita? <i class="fas fa-angle-down rotate-icon"></i>
                        </button>
                    </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionFAQ">
                    <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-md-5 my-auto">
                <div class="embed-responsive embed-responsive-16by9">
                    <video width="444" height="250" controls>
                        <source src="{{ asset('landing/media/nusantara.mp4') }}">
                    </video>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="testimoni">
    <div class="container">
        <h2 class="show-on-scroll">TESTIMONIAL</h2>
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="row">
                    <div class="col-md-10 m-auto">
                        <div class="card text-center">
                            <div class="card-body">
                                <p class="testi-text">" Saya puas dengan fasilitas yang telah disediakan Nusantara GPS. Terutama untuk mantau armada kami yang sedang jalan sangat membantu. Mantap!! "</p>
                                <img src="{{ asset('landing/img/testi1.png') }}" class="img-fluid my-3">
                                <p>Head of indodriver.id</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-10 m-auto">
                        <div class="card text-center">
                            <div class="card-body">
                                <p class="testi-text">" Saya puas dengan fasilitas yang telah disediakan Nusantara GPS. Terutama untuk mantau armada kami yang sedang jalan sangat membantu. Mantap!! "</p>
                                <img src="{{ asset('landing/img/testi1.png') }}" class="img-fluid my-3">
                                <p>Head of drive.co</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-10 m-auto">
                        <div class="card text-center">
                            <div class="card-body">
                                <p class="testi-text">" Saya puas dengan fasilitas yang telah disediakan Nusantara GPS. Terutama untuk mantau armada kami yang sedang jalan sangat membantu. Mantap!! "</p>
                                <img src="{{ asset('landing/img/testi1.png') }}" class="img-fluid my-3">
                                <p>Head of indodriver.id</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>

<section id="contact">
    <div class="container">
        <div class="contact-us">
            <h2 class="show-on-scroll">HUBUNGI KAMI</h2>
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1002.2864624484396!2d112.61623517514171!3d-7.92975391078652!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7883cf943b7df5%3A0x58dfa217cd8c01da!2sNusantara+Gps!5e0!3m2!1sen!2sid!4v1543312307696" width="100%" height="450" frameborder="0" class="text-center"></iframe>
                    <p class="text-center">Untuk meningkatkan pelayanan kami baik kepada customer maupun calon customer, jangan ragu untuk mengirimkan saran maupun kritik melalui form di bawah ini</p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <form action="#" method="POST" id="userform">
                        <div class="form-group">
                            <label for="input-name">Full Name</label>
                            <input type="text" class="form-control" id="input-name" placeholder="Full Name" required>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="input-email">Email</label>
                                <input type="text" class="form-control" id="input-email" placeholder="Email" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="input-telephone">Telephone</label>
                                <input type="number" class="form-control" id="input-telephone" placeholder="Telephone" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea placeholder="Message" class="form-control" id="message" rows="5" form="userform" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="input-captcha">Captcha</label>
                            <input type="number" class="form-control" id="input-captcha" placeholder="Rewrite Captcha" required>
                        </div>
                        <div class="form-group">
                            <div class="button-msg-center text-center">
                                <button type="submit" class="btn btn-primary">Send</button>
                            </div> 
                        </div>                                             
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
