@extends('landing/templates/header')

@section('title', 'Blog | Nusantara GPS')

@section('container')
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-md-6 blog-content">
                <h1>BLOG</h1>
                <p class="my-5">Dapatkan informasi-informasi menarik dan faktual mengenai GPS serta manfaat dari penggunaan GPS melalui artikel dibawah ini</p>
                <div>
                    <form class="form-inline" method="POST" action="">                        
                        <div class="form-group mb-2">
                            <input type="text" class="form-control" id="inputKeywordSearch" placeholder="Search Article by Title">
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Search</button>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <img src="{{ asset('blog/img/blog-hero.png') }}" class="img-fluid mx-auto">
            </div>
        </div>
    </div>
</section>

<section id="post">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('blog/img/cover/polisi.png') }}" alt="Card image cap">
                    <div class="card-body">
                        <p>Selasa, 12 Agustus 2020 <span style="float: right">07:14</span></p>
                        <a href="" class="title">Manfaatkan GPS, Polisi tangkap perampas mobil di Serang</a>
                        <p class="card-text">Kepolisian Polda Metro Jaya bekerja sama dengan jajaran Mapolres Serang, Banten, menangkap pelaku pencurian mobil dengan memanfaatkan teknologi GPS (global.....</p>
                        <a href="#">Selengkapnya</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('blog/img/cover/polisi.png') }}" alt="Card image cap">
                    <div class="card-body">
                        <p>Selasa, 12 Agustus 2020 <span style="float: right">07:14</span></p>
                        <a href="" class="title">Susul As dan Rusia, China luncurkan satelit pendukung...</a>
                        <p class="card-text">Untuk mendukung teknologi sistem posisi global ( GPS), pemerintah China meluncurkan dua satelit bernama BeiDou-3 ke luar angkasa, pada Minggu (5/11/2017), pukul 19.45 waktu.....</p>
                        <a href="#">Selengkapnya</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('blog/img/cover/polisi.png') }}" alt="Card image cap">
                    <div class="card-body">
                        <p>Selasa, 12 Agustus 2020 <span style="float: right">07:14</span></p>
                        <a  href="" class="title">Berkat GPS, Komplotan pencuri mobil tertangkap</a>
                        <p class="card-text">Nasib trio Kar (37), Nar (38), dan Rus (28) kiranya pas dengan peribahasa sepandai-pandai tupai melompat, akhirnya jatuh ke tanah juga. Sudah berulang sukses mencuri mobil, akhirnya tertangkap juga....</p>
                        <a href="#">Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('blog/img/cover/polisi.png') }}" alt="Card image cap">
                    <div class="card-body">
                        <p>Selasa, 12 Agustus 2020 <span style="float: right">07:14</span></p>
                        <a href="#" class="title">Cara Kerja GPS</a>
                        <p class="card-text">Sistem GPS terdiri dari tiga bagian, yakni satelit di angkasa, stasiun pengendali di bumi, dan receiver alias perangkat penerima sinyal satelit yang berada di tangan pengguna, seperti misalnya smartphone atau....</p>
                        <a href="#">Selengkapnya</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('blog/img/cover/polisi.png') }}" alt="Card image cap">
                    <div class="card-body">
                        <p>Selasa, 12 Agustus 2020 <span style="float: right">07:14</span></p>
                        <a href="" class="title">Apa itu GPS?</a>
                        <p class="card-text">Dulu, para pelaut melakukan navigasi dilakukan dengan berpatokan pada tanda-tanda alam seperti posisi bintang di langit. Peta harus dibaca secara manual. Itupun tidak bisa serta merta menentukan di mana....</p>
                        <a href="#">Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection