// Owl carousel
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:10000,
    autoplayHoverPause:true,
    // nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

//slick
$('.carousel').slick({
    centerMode: true,
    centerPadding: '20px',
    slidesToShow: 3,
    arrows: true,
    prevArrow: "<button type='button' class='slick-prev pull-left'><i class='fas fa-arrow-circle-left fa-3x' aria-hidden='true'></i></button>",
    nextArrow: "<button type='button' class='slick-next pull-right'><i class='fas fa-arrow-circle-right fa-3x' aria-hidden='true'></i></button>",
    responsive: [
      {
        breakpoint: 800,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 360,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '60px',
          slidesToShow: 1
        }
      }
    ]
});

//Navbar smooth scrolling javascript
$('.navbar a').on('click', function(e) {
  if(this.hash !== '') {
      e.preventDefault();

      const hash = this.hash;

      $('html, body').animate(
          {
              scrollTop: $(hash).offset().top
          },
          800
      );
  }
});

//navbar active
$(".navbar-light .navbar-nav li").on("click", function() {
    $(".navbar-light .navbar-nav li").removeClass("active");
    $(this).addClass("active");
});

//navbar spyscroll
// $('body').scrollspy({target: ".navbar-light"});